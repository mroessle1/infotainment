# infotainment

A simple Python program running on a Raspberry PI displaying the current measurement status at the KMC-3 XPP endstation of the synchrotron BESSY II in Berlin, Germany. It relies on the [guizero](https://lawsie.github.io/guizero/) Python module for generating the display and reads the values of [EPICS](https://epics-controls.org) process variables using [PyEpics](https://pyepics.github.io/pyepics/).
