#!/usr/bin/python3

def main():
    version = 0.28
    date = '09.06.2023'

    from guizero import App, Box, PushButton, Text, TextBox, Window # load gui framework
    from epics import caget, caput      # load epics connection framework

    def closeWindow():
        app.destroy()
    def configWindow():
        pass
    def refreshWindow():
        pass
    def open_config():
        configWindow.show(wait = True)
    def close_config():
        configWindow.hide()
    def open_about():
        aboutWindow.show(wait = True)
    def close_about():
        configWindow.hide()

    def getDeltaT():
        deltaT = deltaTInput.value
        # print(deltaT)

    def gettauAPD0():
        tauAPD0 = tauAPD0Input.value
        print(tauAPD0)
        return float(tauAPD0)

    # define functions for interaction with epics
    def getBessyTime():
        '''get BESSY current time'''
        currentTime = caget('SYSCC:time')
        bessyTime.value = (currentTime)

    def getBessyCurrent():
        '''get BESSY current value'''
        beamCurrent = caget('CUMZR:rdCur')
        currentValue.value = '{:3.1f} mA'.format(float(beamCurrent))

    def getBessyState():
        '''get operation status of BESSY'''
        status = caget('TOPUPCC:message')
        if status == 1:
            statusString.value = 'top-up'
            statusBox.bg = 'green'
        elif status == 0:
            statusString.value = 'top-up off'
            statusBox.bg = 'orange'
        elif status == 4:
            statusString.value = 'injection'
            statusBox.bg = 'orange'
        elif status == 5:
            statusString.value = 'injection off'
            statusBox.bg = 'orange'
        elif status == 6:
            statusString.value = 'decay mode'
            statusBox.bg = 'red'
        elif status == 9:
            statusString.value = 'decaying beam'
            statusBox.bg = 'orange'
        elif status == 13:
            statusString.value = 'no beam'
            statusBox.bg = 'red'
        else:
            statusString.value = 'unknown'
            statusBox.bg = 'red'

    def getBessyMode():
        '''get BESSY operation mode'''
        modeBessy = caget('FPATCC:mode')
        if modeBessy == 3:
            modeString.value = 'no beam'
            modeBox.bg = 'red'
        elif modeBessy == 2:
            modeString.value = 'hybrid'
            modeBox.bg = 'green'
        elif modeBessy == 1:
            modeString.value = 'multibunch'
            modeBox.bg = 'green'
        elif modeBessy == 0:
            modeString.value = 'single bunch'
            modeBox.bg = 'green'

    def getShutterStatus():
        '''get KMC3 shutter status'''
        shutterStatus = caget('BSR21B013L:State1')
        if shutterStatus == 14:
            shutterBoxValue.value = 'open'
            shutterBox.bg = 'red'
        elif shutterStatus == 1:
            shutterBoxValue.value = 'closed'
            shutterBox.bg = 'green'
        elif shutterStatus == 5:
            shutterBoxValue.value = 'closed'
            shutterBox.bg = 'orange'
        else:
            shutterBoxValue.value = 'ERROR'
            shutterBox.bg = 'red'

    def getLaserSyncState():
        '''get synchronisation status of the pump laser'''
        repRate = caget('KMC-3_XPP:laser_frequency')
        sync = 'True'
        if sync == 'True':
            repRateBox.bg = 'green'
        elif sync == 'False':
            repRateBox.bg = 'orange'
        else:
            repRateBox.bg = 'red'
        repRateValue.value = '{:7.0f} Hz'.format(float(repRate))

    def getSpecScan():
        '''get current SPEC scan number'''
        specScan = caget('KMC-3_XPP:osc_scannum')
        specBoxValue.value = '# {:5.0f}'.format(float(specScan))

    def getPumpProbeDelay():
        '''get power setpoint of the pump laser'''
        ppDelay = caget('KMC-3_XPP:tau_apd')
        ppDelay = -float(ppDelay) + gettauAPD0()
        delayValue.value = '{:4.3f} ns'.format(float(ppDelay))  

    def getLaserPower():
        '''get pump - probe delay in ps'''
        lPower = caget('KMC-3_XPP:laser_power')
        laserPowerValue.value = '{:5.0f} mW'.format(float(lPower * 1000))

    def getSampleTemperature():
        '''get sample temperature in K'''
        sampleTemp = '242'
        setpoint = '241'
        sampleTemp = float(sampleTemp)
        setpoint = float(setpoint)
        if abs(setpoint - sampleTemp) <= 1:
            sampleTemperatureBox.bg = 'green'
        elif (abs(setpoint - sampleTemp) > 1 and abs(setpoint - sampleTemp) < float(deltaTValue)):
            sampleTemperatureBox.bg = 'orange'
        else:
            sampleTemperatureBox.bg = 'red'
        sampleTemperatureValue.value = '{:3.2f} K'.format(float(sampleTemp))

    def getSampleSetpoint():
        '''get sample temperature setpoint in K'''
        setpoint = ' '
        setpointValue.value = '{} K'.format(setpoint)

    def getDCMEnergy():
        '''get current SPEC scan number'''
        DCMSP = caget('DCM1OB013LDCM_ENERGY_SP')
        DCMEnergyValue.value = '{:5.1f} eV'.format(DCMSP)

    # # define functions for interaction with the Edwards rough pump
    # def getEdwardsStatus():
    #     '''get status of Edwards rough pump'''
    #     status = '2'
    #     if status == '1':
    #         roughPumpBoxText.bg = 'green'
    #         roughPumpStart.disable()
    #         roughPumpStop.enable()
    #     elif status == '0':
    #         roughPumpBoxText.bg = 'orange'
    #         roughPumpStart.enable()
    #         roughPumpStop.disable()
    #     else:
    #         roughPumpBoxText.bg = 'red'
    #         roughPumpStart.disable()
    #         roughPumpStop.disable()
    # def startEdwardsPump():
    #     # send !C802\s1\cr (answer !C802\sr\cr)
    #     pass
    # def stopEdwardsPump():
    #     # send !C802\s0\cr (answer !C802\sr\cr)
    #     pass
    # # standby send !C803\s1\cr (answer !C803\sr\cr)

    # # define functions for interaction with Leybold TMP    
    # def getLeyboldStatus():
    #     '''get status of Leybold TMP'''
    #     status = '2'
    #     if status == '1':
    #         tmpPumpBoxText.bg = 'green'
    #         tmpStop.enable()
    #         tmpStart.disable()            
    #     elif status == '0':
    #         tmpPumpBoxText.bg = 'orange'
    #         tmpStart.enable()
    #         tmpStop.disable()            
    #     else:
    #         tmpPumpBoxText.bg = 'red'
    #         tmpStart.disable()
    #         tmpStop.disable()
    # def startLeyboldPump():
    #     pass
    # def stopLeyboldPump():
    #     pass

    deltaTValue = int(5)

    app = App(title = '', width = 1280, height = 1024)
    app.set_full_screen()

    #####
    headerBox = Box(app, width = 'fill', align = 'top')
    header = Text(headerBox, text= 'KMC3-XPP status display')
    headerBox.bg = 'black'
    header.text_color = 'yellow'
    header.text_size = 54
    #####

    fullBox = Box(app, width = 'fill', height = 'fill')

    #####
    bessyBox = Box(fullBox, width = 'fill', height = 'fill', align = 'top', border = True)
    bessyTitleBox = Box(fullBox, width = 'fill', align = 'top')
    label = Text(bessyTitleBox, width = 'fill', text = 'Bessy status', align = 'left')
    label.text_size = 36
    bessyTime = Text(bessyTitleBox, width = 'fill', text = 'time', align = 'right')
    bessyTime.text_size = 36
    bessyTime.repeat(1000, getBessyTime)

    currentBox = Box(bessyBox, width = 'fill', height = 'fill', align = 'left', border = True)
    currentText = Text(currentBox, text = 'Current', align = 'top')
    currentText.text_size = 36
    currentValue = Text(currentBox, text = '0', align = 'bottom')
    currentValue.repeat(2000, getBessyCurrent)
    currentValue.text_size = 42

    modeBox = Box(bessyBox, width = 'fill', height = 'fill', align = 'left', border = True)
    modeText = Text(modeBox, text = 'Operation mode', align = 'top')
    modeText.text_size = 36
    modeString = Text(modeBox, text = '0', align = 'bottom')
    modeString.repeat(10000, getBessyMode)
    modeString.text_size = 42

    statusBox = Box(bessyBox, width = 'fill', height = 'fill', align = 'left', border = True)
    statusText = Text(statusBox, text = 'Status', align = 'top')
    statusText.text_size = 36
    statusString = Text(statusBox, text = '0', align = 'bottom')
    statusString.repeat(10000, getBessyState)
    statusString.text_size = 42

    #####

    #####
    xppBox = Box(fullBox, width = 'fill', height = 'fill', align = 'top', border = True)
    label = Text(xppBox, width = 'fill', text = 'XPP status', align = 'top')
    label.text_size = 36
    leftBox = Box(xppBox, height = 'fill', width = 'fill', align = 'left', border = True)
    shutterBox = Box(leftBox, height = 'fill', width = 'fill', align = 'top', border = True)
    shutterBoxText = Text(shutterBox, text = 'Shutter status', align = 'top')
    shutterBoxText.text_size = 36
    shutterBoxValue = Text(shutterBox, text = '0', align = 'bottom')
    shutterBoxValue.repeat(5000, getShutterStatus)
    shutterBoxValue.text_size = 42

    specBox = Box(leftBox, height = 'fill', width = 'fill', align = 'bottom', border = True)
    specBoxText = Text(specBox, text = 'Current spec scan', align = 'top')
    specBoxText.text_size = 36
    specBoxValue = Text(specBox, text = '0', align = 'bottom')
    specBoxValue.repeat(1000, getSpecScan)
    specBoxValue.text_size = 42

    sampleBox = Box(xppBox, height = 'fill', width = 'fill', align = 'left', border = True)
    sampleTemperatureBox = Box(sampleBox, height = 'fill', width = 'fill', align = 'top', border = True)
    sampleTemperatureText = Text(sampleTemperatureBox, text = 'Sample temperature', align = 'top')
    sampleTemperatureText.text_size = 36
    sampleTemperatureValue = Text(sampleTemperatureBox, text = '0', align = 'bottom')
    sampleTemperatureValue.repeat(2000, getSampleTemperature)
    sampleTemperatureValue.text_size = 42
    setPointBox = Box(sampleBox, height = 'fill', width = 'fill', align = 'top', border = True)
    setpointText = Text(setPointBox, text = 'Setpoint', align = 'top')
    setpointText.text_size = 36
    setpointValue = Text(setPointBox, text = '0', align = 'bottom')
    setpointValue.repeat(1000, getSampleSetpoint)
    setpointValue.text_size = 42

    monoBox = Box(xppBox, height = 'fill', width = 'fill', align = 'left', border = True)
    DCMBox = Box(monoBox, height = 'fill', width = 'fill', align = 'top', border = True)
    DCMBoxText = Text(DCMBox, text = 'DCM Energy', height = 'fill', width = 'fill', align = 'top')
    DCMBoxText.text_size = 36
    DCMEnergyValue = Text(DCMBox, text = '0', align = 'top')
    DCMEnergyValue.repeat(1000, getDCMEnergy)
    DCMEnergyValue.text_size = 42
    DMMBox = Box(monoBox, height = 'fill', width = 'fill', align = 'top', border = True)
    DMMBoxText = Text(DMMBox, text = 'DMM Energy', height = 'fill', width = 'fill', align ='top')
    DMMBoxText.text_size = 36
    DMMEnergyValue = Text(DMMBox, text = 'nan', align = 'top')
    DMMEnergyValue.text_size = 42
#    monoEnergyValue.repeat(1000, getMonoEnergy)
#    monoEnergyValue.text_size = 42
    # roughPumpStart = PushButton(roughPumpBox, height = 'fill', width = 'fill', align = 'left', text = 'START')
    # roughPumpStop = PushButton(roughPumpBox, height = 'fill', width = 'fill', align = 'right', text = 'STOP')
    # tmpPumpBox = Box(pumpBox, height = 'fill', width = 'fill', border = True)
    # tmpPumpBoxText = Text(tmpPumpBox, width = 'fill', height = 'fill', text = 'Leybold TMP')
    # tmpPumpBoxText.repeat(1000, getLeyboldStatus) 
    # tmpStart = PushButton(tmpPumpBox, height = 'fill', width = 'fill', align = 'left', text = 'START')
    # tmpStop = PushButton(tmpPumpBox, height = 'fill', width = 'fill', align = 'right', text = 'STOP')
    #####

    #####
    laserBox = Box(fullBox, width = 'fill', height = 'fill', align = 'top', border = True)
    label = Text(laserBox, width = 'fill', text = 'Laser status', align = 'top')
    label.text_size = 36
    delayBox = Box(laserBox, width = 'fill', height = 'fill', align = 'left', border = True)
    delayText = Text(delayBox, text = 'Pump-Probe delay', align = 'top')
    delayText.text_size = 36
    delayValue = Text(delayBox, text = '0', align = 'bottom')
    delayValue.repeat(1000, getPumpProbeDelay)
    delayValue.text_size = 42

    repRateBox = Box(laserBox, width = 'fill', height = 'fill', align = 'left', border = True)
    repRateText = Text(repRateBox, text = 'Repetition rate', align = 'top')
    repRateText.text_size = 36
    repRateValue = Text(repRateBox, text = '0', align = 'bottom')
    repRateValue.repeat(5000, getLaserSyncState)
    repRateValue.text_size = 42

    laserPowerBox = Box(laserBox, width = 'fill', height = 'fill', align = 'left', border = True)
    laserPowerText = Text(laserPowerBox, text = 'Laser power', align = 'top')
    laserPowerText.text_size = 36
    laserPowerValue = Text(laserPowerBox, text = '0', align = 'bottom')
    laserPowerValue.repeat(5000, getLaserPower)
    laserPowerValue.text_size = 42
    #####

    #####
    footerBox = Box(app, width = 'fill', align = 'bottom', border = True)
    versionText = Text(footerBox, align = 'left', text = 'XPP status display version {:2.2f} from {}'.format(version, date))
    versionText.text_size = 12
#    about = PushButton(footerBox, text = 'about', align = 'right', command = open_about)
    close = PushButton(footerBox, text = 'close', align = 'right', command = closeWindow)
    config = PushButton(footerBox, text = 'config', align = 'right', command = open_config)
#    refresh = PushButton(footerBox, text = 'refresh', align = 'right', command = refreshWindow)
    #####
    
    configWindow = Window(app, title = 'Configuration', layout = 'grid')
    configWindow.hide()
    titleText = Text(configWindow, text = 'Parameters:', align = 'left', grid = [0,0])
    deltaT = Text(configWindow, text = 'Delta T (K) for stable temperature:', align = 'left', grid = [0,1])
    deltaTInput = TextBox(configWindow, align = 'left', grid = [1,1])
#    deltaTInputSave = PushButton(configWindow, align = 'left', grid = [2,1], command = getDeltaT, text = 'set')
    tauAPD0 = Text(configWindow, text = 'tau_apd_0 value (ns):', align = 'left', grid = [0,2])
    tauAPD0Input = TextBox(configWindow, align = 'left', grid = [1,2], text = '1013.46')
#    tauAPD0InputSave = PushButton(configWindow, align = 'left', grid = [2,2], command = gettauAPD0, text = 'set')
#    print(deltaTValue)

#    aboutWindow = Window(app, title = 'About XPP status', layout = 'grid')
#    aboutText = Text(aboutWindow, text = 'Version ', align = 'center')
#    aboutWindow.hide()

    app.display()
    
if __name__ == '__main__':
    # execute only if run as a script
    main()
